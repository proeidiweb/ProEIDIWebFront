import {InjectionToken} from "@angular/core";

export const CODIGO_APP = new InjectionToken<string>("CODIGO_APP");
export const AUTH_API_URL = new InjectionToken<string>("AUTH_API_URL");
export const TOKEN_STORAGE_KEY = new InjectionToken<number>("TOKEN_STORAGE_KEY");
export const CORE_API_URL = new InjectionToken<string>("CORE_API_URL");

export const OPEN_ID_URL = new InjectionToken<number>("OPEN_ID_URL");
export const ADMIN_API_URL = new InjectionToken<string>("ADMIN_API_URL");
