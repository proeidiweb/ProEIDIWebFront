import {Tipo} from "../tipo.model";

export interface User {
  id: number;
  nome: string;
  login: string;
  email: string;
  papelSelecionado: Tipo;
  papeis: Tipo[];
}
