import {User} from "./user.model";
import {Tipo} from "../tipo.model";

export interface AutenticacaoResponse {
  usuario: User;

  papeis: Tipo[];
  papelSelecionado: Tipo;

  errorMessage: string;
  token: string;
}
