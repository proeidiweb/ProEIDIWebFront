export interface AuthToken {
  idToken: string;
  tipo: "mcore" | "proeidi-core";
}
