export interface PageResponse<T> {
  content: Array<T>;
  currentPage: number;
  pageSize: number;
  totalElements: number;
  totalPages: number;
}
