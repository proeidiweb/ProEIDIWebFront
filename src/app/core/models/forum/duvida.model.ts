import {Tipo} from "../tipo.model";

export interface Duvida extends Tipo{
  dataCriado?: Date;
  dataUltimaAtualizacao?: Date;
  ativo?: boolean;
  pessoaCadastro?: Tipo;
  forum?: Tipo;
}
