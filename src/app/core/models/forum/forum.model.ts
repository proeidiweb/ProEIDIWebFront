import {Tipo} from "../tipo.model";

export interface Forum extends Tipo{
  dataCriado?: Date;
  dataUltimaAtualizacao?: Date;
  ativo?: boolean;
  pessoaCadastro?: Tipo;
  pessoaUltimaAtualizacao?: Tipo;
}
