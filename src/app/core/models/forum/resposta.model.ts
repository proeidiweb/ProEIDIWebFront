import {Tipo} from "../tipo.model";

export interface Resposta extends Tipo {
  dataCriado?: Date;
  dataUltimaAtualizacao?: Date;
  ativo?: boolean;
  pessoaCadastro?: Tipo;
  duvida?: Tipo;
}
