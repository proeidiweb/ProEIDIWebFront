import {Tipo} from "../tipo.model";

export interface PessoaListItem extends Tipo {
  cpf?: string;
  email?: string;
  dataNascimento?: Date;
  dataCriado?: Date;
  pessoaCadastro?: Tipo;
  ativo?: boolean;
  telefones?: string[];
}
