import {PessoaListItem} from "./pessoa-list-item.model";

export interface Pessoa extends PessoaListItem {
  matricula?: string;
  dataUltimaAtualizacao?: Date;
}
