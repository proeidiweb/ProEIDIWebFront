import {Tipo} from "../tipo.model";

export interface DisciplinaList extends Tipo {
  descricao?: string;
}
