import {Tipo} from "../tipo.model";

export interface PeriodoMatricula {
  id?: number;
  ativo?: boolean;
  dataInicio?: Date;
  dataFim?: Date;
  dataCadastro?: Date;
  dataUltimaAtualizacao?: Date;
  pessoaCadastro?: Tipo;
  pessoaUltimaAtualizacao?: Tipo;
}
