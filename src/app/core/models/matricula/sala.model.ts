import {Tipo} from "../tipo.model";

export interface Sala extends Tipo {
  descricao?: string;
  ativo?: boolean;
  dataCriado?: Date;
  dataUltimaAtualizacao?: Date;
  pessoaCadastro?: Tipo;
  pessoaUltimaAtualizacao?: Tipo;
}
