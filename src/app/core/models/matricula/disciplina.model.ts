import {DisciplinaList} from "./disciplina-list.model";
import {Tipo} from "../tipo.model";

export interface Disciplina extends DisciplinaList {
  ativo?: boolean;
  cargaHoraria?: number;
  dataCriado?: Date;
  dataUltimaAtualizacao?: Date;
  pessoaCadastro?: Tipo;
  pessoaUltimaAtualizacao?: Tipo;
}
