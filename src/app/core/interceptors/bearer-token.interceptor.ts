import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { first, mergeMap } from "rxjs/operators";
import { AuthenticationService } from "../services/auth/authentication.service";

@Injectable()
export class BearerAuthInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authenticationService.authToken$.pipe(
      first(),
      mergeMap(token => {
        let authReq = request;

        if (token) {
          const headers = new HttpHeaders().set("Authorization", `Bearer ${token.idToken}`);

          authReq = request.clone({
            headers
          });
        }

        return next.handle(authReq);
      })
    );
  }
}
