import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { NzMessageService } from "ng-zorro-antd";
import { AuthenticationService } from "../services/auth/authentication.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService, private message: NzMessageService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(err => {
        if (err.status === 401) {
          this.message.error("Usuário ou senha incorretos");
          this.authenticationService.logout();
          return throwError(err.error);
        } else if (err.status === 403) {
          this.message.error(err.error.message || "Acesso Negado");
          this.authenticationService.logout();
          return throwError(err.error);
        } else if (err.error.message.includes("JWT expired")) {
          this.message.error("Acesso negadoo");
          this.authenticationService.logout();
        }

        const error = err.error;

        if (error && error.hasOwnProperty("empty") && !error.empty) {
          if (error.errorPresent) {
            error.errorMessages.forEach(it => {
              this.message.error(it.mensagem);
            });
          }

          if (error.warningPresent) {
            error.warningMessages.forEach(it => {
              this.message.warning(it.mensagem);
            });
          }

          if (error.infoPresent) {
            error.infoMessages.forEach(it => {
              this.message.info(it.mensagem);
            });
          }
        } else {
          this.message.error("Ocorreu um erro ao processar o comando. Tente novamente.");
        }

        return throwError(error);
      })
    );
  }
}
