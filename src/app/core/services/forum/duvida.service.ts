import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Duvida} from "../../models/forum/duvida.model";
import {PageResponse} from "../../models/page-response.model";

@Injectable()
export class DuvidaService{
  private duvidaUrl = "/core/v1/duvida";
  constructor(private http: HttpClient) {}

  public save(duvida: Duvida): Observable<number> {
    return this.http.post<number>(this.duvidaUrl, duvida);
  }

  public delete(duvida: number): Observable<void> {
    return this.http.delete<void>(`${this.duvidaUrl}/${duvida}`);
  }

  public getAll(idForum: number, search?: string, page?: number, pageSize?: number): Observable<PageResponse<Duvida>> {
    const params = new HttpParams()
      .set("id_forum", idForum.toString())
      .set("search", search)
      .set("page", page.toString())
      .set("pageSize", pageSize.toString());
    return this.http.get<PageResponse<Duvida>>(this.duvidaUrl, {params});
  }
}
