import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Duvida} from "../../models/forum/duvida.model";
import {PageResponse} from "../../models/page-response.model";
import {Resposta} from "../../models/forum/resposta.model";

@Injectable()
export class RespostaService {
  private respostaUrl = "/core/v1/resposta";

  constructor(
    private http: HttpClient
  ) {}

  public save(resposta: Resposta): Observable<number> {
    return this.http.post<number>(this.respostaUrl, resposta);
  }

  public delete(resposta: number): Observable<void> {
    return this.http.delete<void>(`${this.respostaUrl}/${resposta}`);
  }

  public getAll(
    idDuvida: number,
    search?: string,
    page?: number,
    pageSize?: number
  ): Observable<PageResponse<Resposta>> {
    const params = new HttpParams()
      .set("id_duvida", idDuvida.toString())
      .set("search", search)
      .set("page", page.toString())
      .set("pageSize", pageSize.toString());
    return this.http.get<PageResponse<Resposta>>(this.respostaUrl, {params});
  }
}
