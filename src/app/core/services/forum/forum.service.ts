import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Tipo} from "../../models/tipo.model";
import {HttpClient} from "@angular/common/http";
import {Forum} from "../../models/forum/forum.model";

@Injectable()
export class ForumService {
  private forumUrl = "/core/v1/forum";

  constructor(private http: HttpClient) {
  }

  public save(forum: Forum): Observable<number> {
    return this.http.post<number>(this.forumUrl, forum);
  }

  public delete(forum: number): Observable<void> {
    return this.http.delete<void>(`${this.forumUrl}/${forum}`);
  }

  public getAll(): Observable<Tipo[]> {
    return this.http.get<Tipo[]>(this.forumUrl);
  }
}
