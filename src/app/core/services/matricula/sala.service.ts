import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Tipo} from "../../models/tipo.model";
import {PageResponse} from "../../models/page-response.model";
import {Sala} from "../../models/matricula/sala.model";

@Injectable()
export class SalaService {
  private salaUrl = "/core/v1/sala";

  constructor(
    private http: HttpClient
  ) {
  }

  findAll(): Observable<Tipo[]> {
    return this.http.get<Tipo[]>(this.salaUrl);
  }

  findAllPageable(page: number = 1, pageSize: number = 20, search: string = null): Observable<PageResponse<Sala>> {
    let params = new HttpParams()
      .set("page", page.toString())
      .set("pageSize", pageSize.toString());

    if (!!search) {
      params = params.set("search", search);
    }

    return this.http.get<PageResponse<Sala>>(`${this.salaUrl}/paginado`, {params});
  }

  findOne(idSala: number): Observable<Sala> {
    return this.http.get<Sala>(`${this.salaUrl}/${idSala}`);
  }

  save(sala: Sala): Observable<number> {
    return this.http.post<number>(this.salaUrl, sala);
  }

  desativar(idSala: number): Observable<void> {
    return this.http.delete<void>(`${this.salaUrl}/${idSala}`);
  }
}
