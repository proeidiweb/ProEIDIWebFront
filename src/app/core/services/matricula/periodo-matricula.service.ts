import {Injectable} from "@angular/core";
import {EMPTY, Observable} from "rxjs";
import {PeriodoMatricula} from "../../models/matricula/periodo-matricula.model";
import {HttpClient, HttpParams} from "@angular/common/http";
import {PageResponse} from "../../models/page-response.model";

@Injectable()
export class PeriodoMatriculaService {
  private periodoMatriculaUrl = "/core/v1/periodo-matricula";

  constructor(
    private http: HttpClient
  ) {}

  findAll(page?: number, pageSize?: number): Observable<PageResponse<PeriodoMatricula>> {
    const params = new HttpParams()
      .set("page", page.toString())
      .set("pageSize", pageSize.toString());

    return this.http.get<PageResponse<PeriodoMatricula>>(`${this.periodoMatriculaUrl}`, {params});
  }

  findOne(idPeriodoMatricula: number): Observable<PeriodoMatricula> {
    return this.http.get<PeriodoMatricula>(`${this.periodoMatriculaUrl}/${idPeriodoMatricula}`);
  }

  save(periodo: PeriodoMatricula): Observable<number> {
    return this.http.post<number>(`${this.periodoMatriculaUrl}`, periodo);
  }

  desativar(idPeriodoMatricula: number): Observable<void> {
    return this.http.delete<void>(`${this.periodoMatriculaUrl}/${idPeriodoMatricula}`);
  }
}
