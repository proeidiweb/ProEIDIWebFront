import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Tipo} from "../../models/tipo.model";
import {PageResponse} from "../../models/page-response.model";
import {Sala} from "../../models/matricula/sala.model";
import {DisciplinaList} from "../../models/matricula/disciplina-list.model";
import {Disciplina} from "../../models/matricula/disciplina.model";

@Injectable()
export class DisciplinaService {
  private disciplinaUrl = "/core/v1/disciplina";

  constructor(
    private http: HttpClient
  ) {}

  findAll(): Observable<DisciplinaList[]> {
    return this.http.get<Tipo[]>(this.disciplinaUrl);
  }

  findAllPageable(page: number = 1, pageSize: number = 20, search: string = null): Observable<PageResponse<Disciplina>> {
    let params = new HttpParams()
      .set("page", page.toString())
      .set("pageSize", pageSize.toString());

    if (!!search) {
      params = params.set("search", search);
    }

    return this.http.get<PageResponse<Disciplina>>(`${this.disciplinaUrl}/paginado`, {params});
  }

  findOne(id: number): Observable<Disciplina> {
    return this.http.get<Disciplina>(`${this.disciplinaUrl}/${id}`);
  }

  save(disciplina: Disciplina): Observable<number> {
    return this.http.post<number>(this.disciplinaUrl, disciplina);
  }

  desativar(id: number): Observable<void> {
    return this.http.delete<void>(`${this.disciplinaUrl}/${id}`);
  }
}
