import {Inject, Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {AuthToken} from "../../models/auth/auth-token.model";
import {CODIGO_APP, TOKEN_STORAGE_KEY} from "../../core.constants";
import {Router} from "@angular/router";
import {exhaustMap, filter, first, tap} from "rxjs/operators";
import {Tipo} from "../../models/tipo.model";
import {AutenticacaoResponse} from "../../models/auth/auth-response.model";
import {User} from "../../models/auth/user.model";

@Injectable({providedIn: "root"})
export class AuthenticationService {
  private authUrl = "core/v1/auth";
  private readonly currentUserSubject$: BehaviorSubject<User> = new BehaviorSubject(null);
  private readonly authTokenSubject$: BehaviorSubject<AuthToken>;
  private readonly papeisSubject$ = new BehaviorSubject<Tipo[]>([]);
  private readonly _papelAtual$ = new BehaviorSubject<Tipo>(null);

  constructor(
    private http: HttpClient,
    @Inject(CODIGO_APP) private codigoApp: string,
    @Inject(TOKEN_STORAGE_KEY) private tokenStorageKey: string,
    private router: Router
  ) {
    this.authTokenSubject$ = new BehaviorSubject(null);

    this.autoLogin();

    this.authTokenSubject$.pipe(tap(console.log)).subscribe(it => {
      if (!it) {
        this.router.navigateByUrl("/auth/login");
      } else {
        if (!this._papelAtual$.value) {
          this.router.navigateByUrl("/auth/papeis");
        }
      }

      if (it) {
        localStorage.setItem(this.tokenStorageKey, JSON.stringify(it));
      }
    });
  }

  get papelAtual$() {
    return this._papelAtual$.asObservable();
  }

  get isCoordenador() {
    return this._papelAtual$.value ? this._papelAtual$.value.nome === "Coordenador" : false;
  }

  get currentUser$() {
    return this.currentUserSubject$.asObservable();
  }

  get authToken$() {
    return this.authTokenSubject$.asObservable();
  }

  get papeis$() {
    if (!this.papeisSubject$.getValue().length) {
      this.loadPapeis();
    }

    return this.papeisSubject$.asObservable();
  }

  private loadPapeis() {
    this.http.get<Tipo[]>(`${this.authUrl}/papeis`).subscribe(it => {
      this.papeisSubject$.next(it);
    });
  }

  login(login: string, password: string, remember: boolean = false) {
    const payload = {
      login,
      password
    };

    return this.http
      .post<AutenticacaoResponse>(`${this.authUrl}/login`, {}, {params: {...payload}})
      .pipe(
        tap(
          it => {
            this.papeisSubject$.next(it.papeis);
            localStorage.setItem("usuario", JSON.stringify(it.usuario));
            localStorage.setItem("papeis", JSON.stringify(it.papeis));
            this.currentUserSubject$.next(it.usuario);
            if (!!it.papelSelecionado) {
              localStorage.setItem("papel", JSON.stringify(it.papelSelecionado));
              this._papelAtual$.next(it.papelSelecionado);
              this.router.navigateByUrl("/app/home");
            }
            if (!!this._papelAtual$.value) {
              this.router.navigateByUrl("/app/home");
            }
            this.authTokenSubject$.next({tipo: "proeidi-core", idToken: it.token});
          },
          err => {
            this.currentUserSubject$.next(null);
            this.authTokenSubject$.next(null);
            return err;
          }
        )
      );
  }

  selecionarPapel(idPapel: number) {
    const params = new HttpParams().set("id_papel", idPapel.toString());

    return this.http.post<AutenticacaoResponse>(
      `${this.authUrl}/selecionar-papel`, {}, {params})
      .pipe(
        tap(next => {
          this._papelAtual$.next(next.papelSelecionado);
          this.currentUserSubject$.next(next.usuario);
          // Salvando informações
          localStorage.setItem("usuario", JSON.stringify(next.usuario));
          localStorage.setItem("papeis", JSON.stringify(next.papeis));
          localStorage.setItem("papel", JSON.stringify(next.papelSelecionado));

          this.authTokenSubject$.next({tipo: "proeidi-core", idToken: next.token});
          this.router.navigateByUrl("/app/home");
        }, _ => {
          console.log("Erro ao selecionar papel");
        }));
  }

  public logout() {
    this.currentUserSubject$.next(null);
    this.authTokenSubject$.next(null);
    this.papeisSubject$.next(null);
    this._papelAtual$.next(null);
    localStorage.removeItem(this.tokenStorageKey);
    localStorage.removeItem("usuario");
    localStorage.removeItem("papel");
    localStorage.removeItem("papeis");
    this.router.navigateByUrl("/auth/login");
  }

  private autoLogin() {
    const currentUser  = localStorage.getItem("usuario");
    const storedUser = currentUser ? JSON.parse(currentUser) : null;
    if (!!storedUser) {
      this.currentUserSubject$.next(storedUser);
    }

    const currentPapel = localStorage.getItem("papel");
    const storedPapel = currentPapel ? JSON.parse(currentPapel) : null;
    if (!!storedPapel) {
      this._papelAtual$.next(storedPapel);
    }

    const currentPapeis = localStorage.getItem("papeis");
    const storedPapeis = currentPapeis ? JSON.parse(currentPapeis) : [];
    if (!!storedPapeis) {
      this.papeisSubject$.next(storedPapeis);
    }

    const stringToken = localStorage.getItem(this.tokenStorageKey) || sessionStorage.getItem(this.tokenStorageKey);
    const storedToken = stringToken ? JSON.parse(stringToken) : null;

    if (!!storedToken) {
      this.authTokenSubject$.next(storedToken);
    }
  }
}
