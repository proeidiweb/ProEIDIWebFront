import {Injectable} from "@angular/core";

@Injectable()
export class ValidateService {
  constructor() {}

  static readonly TAMANHO_CPF = 14;

  public formatarRG(dados: string): string {
    if (!dados) {
      return null;
    }

    let rg = dados.replace(/\D/g, "");
    rg = rg.replace(/(\d{3})(\d)/, "$1.$2");
    rg = rg.replace(/(\d{3})(\d)/, "$1.$2");

    return rg;
  }

  public formatarCPF(dados: string): string {
    if (!dados) {
      return null;
    }

    let cpf = this.formatarRG(dados);
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2");

    return cpf;
  }

  public validarCPF(cpf: string): boolean {
    const strCPF = cpf.replace(/\D/g, "");
    let soma = 0;
    let resto = 0;

    if (strCPF === "00000000000") {
      return false;
    }

    for (let j = 1; j <= 2; j++) {
      soma = 0;
      for (let i = 1; i <= 9; i++) {
        soma = soma + parseInt(strCPF.substring(i - 1, i), 10) * (10 + j - i);
      }

      resto = (soma * 10) % 11;

      if (resto === 10 || resto === 11) {
        resto = 0;
      }

      if (resto !== parseInt(strCPF.substring(8 + j, 9 + j), 10)) {
        return false;
      }
    }

    return true;
  }

  public formatarTelefone(tel: string): string {
    if (!!tel) {
      tel = tel.replace(/\D/g, "");
      tel = tel.replace(/^(\d)/, "($1");
      tel = tel.replace(/(.{3})(\d)/, "$1) $2");

      if (tel.length === 9) {
        tel.replace(/(.{1})$/, "-$1");
      } else if (tel.length === 10) {
        tel.replace(/(.{2})$/, "-$1");
      } else if (tel.length === 11) {
        tel.replace(/(.{3})$/, "-$1");
      } else if (tel.length > 11) {
        tel.replace(/(.{4})$/, "-$1");
      }
    }
    return tel;
  }
}
