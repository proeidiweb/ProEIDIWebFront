import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {PageResponse} from "../../models/page-response.model";
import {PessoaListItem} from "../../models/pessoa/pessoa-list-item.model";
import {Observable} from "rxjs";
import {Pessoa} from "../../models/pessoa/pessoa.model";

@Injectable()
export class PessoaService {
  private pessoaUrl = "/core/v1/pessoa";

  constructor(private http: HttpClient) {}

  findAll(somenteMonitores: boolean = true, search?: string, page?: number, pageSize?: number): Observable<PageResponse<PessoaListItem>> {
    let params = new HttpParams()
      .set("monitores", String(somenteMonitores))
      .set("page", page.toString())
      .set("pageSize", pageSize.toString());

    if (!!search) {
      params = params.set("search", search);
    }

    return this.http.get<PageResponse<PessoaListItem>>(this.pessoaUrl, {params});
  }

  findOne(idPessoa: number): Observable<Pessoa> {
    return this.http.get<Pessoa>(`${this.pessoaUrl}/${idPessoa}`);
  }

  saveMonitor(pessoa: Pessoa): Observable<number> {
    return this.http.post<number>(`${this.pessoaUrl}/monitor`, pessoa);
  }

  deleteMonitor(idPessoa: number): Observable<void> {
    return this.http.delete<void>(`${this.pessoaUrl}/${idPessoa}`);
  }

  reativar(idPessoa: number): Observable<void> {
    return this.http.get<void>(`${this.pessoaUrl}/${idPessoa}/ativar`);
  }
}
