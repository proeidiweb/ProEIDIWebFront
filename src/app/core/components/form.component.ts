import {FormGroup} from "@angular/forms";
import {EventEmitter, Input, Output} from "@angular/core";
import {Observable} from "rxjs";
import {NzMessageService} from "ng-zorro-antd";

export abstract class FormComponent<T> {
  // Formulário
  public formulario: FormGroup;

  // Booleans
  public isVisible = false;
  public isSalvando = false;
  public isEditando = false;

  // MessageBus
  @Input() openForm: Observable<T>;
  @Output() emitEvent = new EventEmitter();

  constructor(
    protected message: NzMessageService
  ) {}

  construirObjeto() {
    Object.values(this.formulario.controls).forEach(it => {
      it.markAsDirty();
      it.updateValueAndValidity();
    });

    if (this.formulario.invalid) {
      this.message.error("Formulário inválido");
      return null;
    }

    const params = this.formulario.getRawValue();
    this.isSalvando = true;
    const objeto: T = {
      ...params
    };

    return objeto;
  }

  close(emit?) {
    this.isVisible = false;
    this.isSalvando = false;
    this.isEditando = false;
    this.emitEvent.emit(emit);
  }
}
