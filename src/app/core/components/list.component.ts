import {FormGroup} from "@angular/forms";
import {BehaviorSubject} from "rxjs";

export abstract class ListComponent<T> {
  // Filtro Form
  public filtroForm: FormGroup;

  // Estrutura de Dados
  public dados: T[];

  // Booleans
  public isCarregando = false;

  // Controle da Tabela
  public search: string = null;
  public currentPage = 1;
  public pageSize = 10;
  public totalElements: number;
  public totalPages: number;

  // MessageBus
  public openForm$ = new BehaviorSubject<T>(null);

  constructor() {}

  public abstract openForm(t?: T);
}
