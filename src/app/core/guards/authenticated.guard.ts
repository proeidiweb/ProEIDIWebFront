import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {first, map} from "rxjs/operators";
import {AuthenticationService} from "../services/auth/authentication.service";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class AuthenticatedGuard implements CanActivate, CanActivateChild {

  constructor(private router: Router, private authService: AuthenticationService) {
  }

  authenticated(route: ActivatedRouteSnapshot): Observable<boolean> {
    const anyToken = (route.data && route.data.auth && route.data.auth.anyToken) || false;
    const tipoToken = (route.data && route.data.auth && route.data.auth.tipoToken) || "proeidi-core";

    return this.authService.authToken$.pipe(
      map(it => {
        if (!it) {
          this.router.navigate(["/login"]);
        }

        return !!it && (anyToken || it.tipo === tipoToken);
      }),
      first()
    );
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authenticated(route);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authenticated(childRoute);
  }

}
