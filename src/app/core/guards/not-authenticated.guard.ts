import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment} from "@angular/router";
import {Observable} from "rxjs";
import {AuthenticationService} from "../services/auth/authentication.service";
import {first, map} from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class NotAuthenticatedGuard implements CanActivate, CanLoad {
  constructor(private router: Router, private authService: AuthenticationService) {
  }

  notAuthenticated(): Observable<boolean> {
    return this.authService.authToken$.pipe(
      map(it => {
        if (it && it.tipo === "proeidi-core") {
          this.router.navigate(["/app"]);
        }

        return !it || it.tipo !== "proeidi-core";
      }),
      first()
    );
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return undefined;
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> {
    return undefined;
  }

}
