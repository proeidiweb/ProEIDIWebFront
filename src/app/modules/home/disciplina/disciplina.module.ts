import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {
  NzButtonModule,
  NzFormModule,
  NzIconModule,
  NzInputModule, NzInputNumberModule,
  NzModalModule,
  NzPopconfirmModule,
  NzSpinModule,
  NzTableModule, NzToolTipModule
} from "ng-zorro-antd";
import {ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {DisciplinaComponent} from "./disciplina.component";
import {DisciplinaService} from "../../../core/services/matricula/disciplina.service";
import {DisciplinaFormComponent} from "./disciplina-form/disciplina-form.component";

const routes: Routes = [
  {
    path: "",
    component: DisciplinaComponent,
    children: []
  }
];

@NgModule({
  declarations: [
    DisciplinaComponent,
    DisciplinaFormComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    ReactiveFormsModule,
    NzTableModule,
    NzPopconfirmModule,
    NzIconModule,
    CommonModule,
    NzModalModule,
    NzSpinModule,
    NzInputNumberModule,
    NzToolTipModule
  ],
  providers: [
    DisciplinaService
  ]
})
export class DisciplinaModule {
  constructor() {}
}
