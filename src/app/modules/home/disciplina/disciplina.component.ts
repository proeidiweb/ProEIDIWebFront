import {Component, OnInit} from "@angular/core";
import {ListComponent} from "../../../core/components/list.component";
import {Disciplina} from "../../../core/models/matricula/disciplina.model";
import {NzMessageService} from "ng-zorro-antd";
import {DisciplinaService} from "../../../core/services/matricula/disciplina.service";
import {tap} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: "app-disciplina",
  templateUrl: "./disciplina.component.html",
  styleUrls: ["./disciplina.component.less"]
})
export class DisciplinaComponent extends ListComponent<Disciplina> implements OnInit {

  constructor(
    private message: NzMessageService,
    private fb: FormBuilder,
    private disciplinaService: DisciplinaService
  ) {
    super();
  }

  ngOnInit() {
    this.initializeForm();
    this.carregarDados(1);
  }

  initializeForm() {
    this.filtroForm = this.fb.group({
      search: [null]
    });

    this.filtroForm.get("search").valueChanges.subscribe(next => {
      if (!!next && next.length > 0) {
        this.search = next;
      } else {
        this.search = null;
      }
      this.carregarDados(1);
    });
  }

  carregarDados(current: number, resetIndex: boolean = false) {
    if (!!resetIndex) {
      this.currentPage = 1;
      this.pageSize = current;
    } else {
      this.currentPage = current;
    }

    this.isCarregando = true;

    this.disciplinaService.findAllPageable(this.currentPage, this.pageSize, this.search).pipe(
      tap(response => {
        this.totalElements = response.totalElements;
        this.totalPages = response.totalPages;
        this.dados = response.content;
        this.isCarregando = false;
      }, err => {
        this.message.error("Erro ao recuperar as disciplinas");
        this.isCarregando = false;
        console.log(err.message);
      })).subscribe();
  }

  openForm(disciplina?: Disciplina) {
    if (!disciplina) {
      disciplina = {};
    }
    this.openForm$.next(disciplina);
  }

  desativar(id: number) {
    this.disciplinaService.desativar(id).pipe(tap(_ => {
      this.message.success("Disciplina desativada");
      this.carregarDados(1);
    }, _ => {
      this.message.error("Tente novamente");
      this.carregarDados(1);
    })).subscribe();
  }
}
