import {FormComponent} from "../../../../core/components/form.component";
import {Disciplina} from "../../../../core/models/matricula/disciplina.model";
import {Component, OnInit} from "@angular/core";
import {NzMessageService} from "ng-zorro-antd";
import {FormBuilder, Validators} from "@angular/forms";
import {DisciplinaService} from "../../../../core/services/matricula/disciplina.service";
import {tap} from "rxjs/operators";

@Component({
  selector: "app-disciplina-form",
  templateUrl: "./disciplina-form.component.html",
  styleUrls: ["./disciplina-form.component.less"]
})
export class DisciplinaFormComponent extends FormComponent<Disciplina> implements OnInit {

  constructor(
    private fb: FormBuilder,
    protected message: NzMessageService,
    private disciplinaService: DisciplinaService
  ) {
    super(message);
  }

  ngOnInit(): void {
    this.initializeForm();

    this.openForm.subscribe((next) => {
      if (!next) {
        return;
      }

      this.initializeForm();
      if (!!next.id) {
        this.formulario.patchValue({...next}, {emitEvent: false});
        this.isEditando = true;
      }
      this.isVisible = true;
    });
  }

  initializeForm() {
    this.formulario = this.fb.group({
      id: [null],
      nome: [null, Validators.required],
      descricao: [null, Validators.required],
      cargaHoraria: [null, Validators.required]
    });
  }

  save() {
    const disciplina = super.construirObjeto();
    if (!disciplina) {
      return;
    }

    this.disciplinaService.save(disciplina).pipe(tap(_ => {
      this.message.success("Disciplina salva");
      this.close();
    }, err => {
      this.message.error(err.message);
      this.isSalvando = false;
    })).subscribe();
  }

  desativar() {
    const idSala = this.formulario.getRawValue().id;
    this.isSalvando = true;
    this.disciplinaService.desativar(idSala).pipe(tap(_ => {
      this.message.success("Disciplina desativada");
      this.close();
    }, err => {
      this.message.error("Tente novamente");
      console.log(err.message);
    })).subscribe();
  }

  close() {
    this.initializeForm();
    super.close();
  }

}
