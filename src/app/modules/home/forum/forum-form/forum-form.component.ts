import {Component, OnInit} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";
import {tap} from "rxjs/operators";
import {ForumService} from "../../../../core/services/forum/forum.service";
import {NzMessageService} from "ng-zorro-antd";
import {Tipo} from "../../../../core/models/tipo.model";
import {FormComponent} from "../../../../core/components/form.component";

@Component({
  selector: "app-forum-form",
  templateUrl: "./forum-form.component.html",
  styleUrls: ["./forum-form.component.less"]
})
export class ForumFormComponent extends FormComponent<Tipo> implements OnInit {

  constructor(
    private fb: FormBuilder,
    private forumService: ForumService,
    protected message: NzMessageService
  ) {
    super(message);
  }

  ngOnInit(): void {
    this.initializeForumForm();

    this.openForm.subscribe((next) => {
      if (!next) {
        return;
      }

      this.initializeForumForm();
      if (!!next.id) {
        this.isEditando = true;
        this.formulario.patchValue({...next});
      }
      this.isVisible = true;
    });
  }

  initializeForumForm() {
    this.formulario = this.fb.group({
      id: [null],
      nome: [null, [Validators.required]]
    });
  }

  close() {
    this.initializeForumForm();
    super.close();
  }

  save() {
    const forum = super.construirObjeto();
    if (!forum) {
      return;
    }

    this.forumService.save(forum)
      .pipe(
        tap(_ => {
            this.close();
            this.message.success("Tópico de fórum salvo");
          },
          _ => {
            this.message.error("Erro ao salvar o tópico de fórum");
            this.isSalvando = false;
          })
      ).subscribe();
  }

  delete() {
    this.isSalvando = true;
    const params = this.formulario.getRawValue();
    this.forumService.delete(params.id).pipe(tap(_ => {
      this.message.success("Tópico de fórum removido");
      this.close();
    }, _ => {
      this.isSalvando = false;
      this.message.error("Erro ao remover tópico de fórum");
    })).subscribe();
  }
}
