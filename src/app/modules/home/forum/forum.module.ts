import {NgModule} from "@angular/core";
import {ForumComponent} from "./forum.component";
import {RouterModule, Routes} from "@angular/router";
import {ForumService} from "../../../core/services/forum/forum.service";
import {
  NzAvatarModule,
  NzButtonModule, NzCommentModule,
  NzFormModule,
  NzIconModule, NzInputModule,
  NzMenuModule,
  NzModalModule, NzPopconfirmModule,
  NzSpinModule, NzTableModule,
  NzToolTipModule
} from "ng-zorro-antd";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";
import {DuvidaComponent} from "./duvida/duvida.component";
import {DuvidaService} from "../../../core/services/forum/duvida.service";
import {DuvidaFormComponent} from "./duvida/duvida-form/duvida-form.component";
import {ForumFormComponent} from "./forum-form/forum-form.component";
import {RespostaFormComponent} from "./resposta/resposta-form/resposta-form.component";
import {RespostaService} from "../../../core/services/forum/resposta.service";
import {RespostaComponent} from "./resposta/resposta.component";

const routes: Routes = [
  {
    path: "",
    component: ForumComponent,
    children: []
  }
];

@NgModule({
  declarations: [
    ForumComponent,
    ForumFormComponent,
    DuvidaComponent,
    DuvidaFormComponent,
    RespostaComponent,
    RespostaFormComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    NzMenuModule,
    CommonModule,
    NzIconModule,
    NzToolTipModule,
    NzModalModule,
    NzButtonModule,
    NzSpinModule,
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzTableModule,
    NzPopconfirmModule,
    NzCommentModule,
    NzAvatarModule
  ],
  providers: [
    ForumService,
    DuvidaService,
    RespostaService
  ]
})
export class ForumModule {}
