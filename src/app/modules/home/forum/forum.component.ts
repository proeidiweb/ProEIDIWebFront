import {Component, OnInit} from "@angular/core";
import {ForumService} from "../../../core/services/forum/forum.service";
import {Tipo} from "../../../core/models/tipo.model";
import {NzMessageService} from "ng-zorro-antd";
import {AuthenticationService} from "../../../core/services/auth/authentication.service";
import {Subject} from "rxjs";
import {ListComponent} from "../../../core/components/list.component";

@Component({
  selector: "app-forum",
  templateUrl: "./forum.component.html",
  styleUrls: ["./forum.component.less"]
})
export class ForumComponent extends ListComponent<Tipo> implements OnInit {
  // Estrutura de Dados
  forumEscolhido: Tipo;

  // MessageBus
  forumEscolhido$ = new Subject<Tipo>();

  constructor(
    private forumService: ForumService,
    private message: NzMessageService,
    private auth: AuthenticationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.loadForuns();
  }

  loadForuns() {
    this.forumService.getAll().subscribe(next => {
      this.dados = next;
    }, _ => (this.message.error("Erro ao recuperar os Fóruns")));
  }

  recuperarDuvidas(forum: Tipo) {
    this.forumEscolhido = forum;
    this.forumEscolhido$.next(forum);
  }

  openForm(forum?: Tipo) {
    if (!forum) {
      forum = {};
    }
    this.openForm$.next(forum);
  }

  openModalEditarForum(forum: Tipo) {
    if (this.isCoordenador) {
      this.openForm(forum);
    }
  }

  get isCoordenador() {
    return this.auth.isCoordenador;
  }
}
