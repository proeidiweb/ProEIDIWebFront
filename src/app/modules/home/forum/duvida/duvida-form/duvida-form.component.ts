import {Component, OnInit} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";
import {Tipo} from "../../../../../core/models/tipo.model";
import {Duvida} from "../../../../../core/models/forum/duvida.model";
import {tap} from "rxjs/operators";
import {DuvidaService} from "../../../../../core/services/forum/duvida.service";
import {NzMessageService} from "ng-zorro-antd";
import {FormComponent} from "../../../../../core/components/form.component";

@Component({
  selector: "app-duvida-form",
  templateUrl: "./duvida-form.component.html",
  styleUrls: ["./duvida-form.component.less"]
})
export class DuvidaFormComponent extends FormComponent<Duvida> implements OnInit {
  // Estrutura de Dados
  forum: Tipo;

  constructor(
    private fb: FormBuilder,
    private duvidaService: DuvidaService,
    protected message: NzMessageService
  ) {
    super(message);
  }

  ngOnInit() {
    this.initializeForm();
    this.openForm.subscribe((next) => {
      if (!next) {
        return;
      }

      this.forum = next.forum;
      if (!next.id) {
        this.initializeForm();
      } else {
        this.isEditando = true;
        this.formulario.patchValue({
          id: next.id,
          nome: next.nome
        }, {emitEvent: false});
      }
      this.isVisible = true;
    });
  }

  initializeForm() {
    this.formulario = this.fb.group({
      id: [null],
      nome: [null, Validators.required]
    });
  }

  closeModal() {
    this.initializeForm();
    super.close();
  }

  save() {
    const duvida = super.construirObjeto();

    if (!duvida) {
      return;
    }

    duvida.forum = this.forum;

    this.duvidaService.save(duvida).pipe(tap(_ => {
      this.message.success("Dúvida salva");
      this.closeModal();
    }, _ => {
      this.message.error("Erro ao salvar dúvida");
      this.isSalvando = false;
    })).subscribe();
  }

  delete() {
    this.isSalvando = true;
    const params = this.formulario.getRawValue();
    if (!params.id && this.isEditando) {
      this.duvidaService.delete(params.id).pipe(tap(_ => {
        this.message.success("Dúvida removida");
        this.closeModal();
      }, _ => {
        this.message.error("Erro ao remover dúvida");
        this.isSalvando = false;
      })).subscribe();
    }
  }
}
