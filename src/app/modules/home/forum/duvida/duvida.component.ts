import {Component, Input, OnInit} from "@angular/core";
import {Duvida} from "../../../../core/models/forum/duvida.model";
import {DuvidaService} from "../../../../core/services/forum/duvida.service";
import {tap} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd";
import {Tipo} from "../../../../core/models/tipo.model";
import {BehaviorSubject, Observable} from "rxjs";
import {AuthenticationService} from "../../../../core/services/auth/authentication.service";
import {User} from "../../../../core/models/auth/user.model";
import {Resposta} from "../../../../core/models/forum/resposta.model";
import {ListComponent} from "../../../../core/components/list.component";

@Component({
  selector: "app-duvida",
  templateUrl: "./duvida.component.html",
  styleUrls: ["./duvida.component.less"]
})
export class DuvidaComponent extends ListComponent<Duvida> implements OnInit {
  // Input
  forum: Tipo;

  // MessageBus
  @Input() forumEscolhido$: Observable<Tipo>;
  respostaCadastro$ = new BehaviorSubject<Resposta>(null);
  duvidaEscolhida$ = new BehaviorSubject<Duvida>({});

  // Estrutura de Dados
  usuario: User;

  constructor(
    private duvidaService: DuvidaService,
    private auth: AuthenticationService,
    private message: NzMessageService
  ) {
    super();
  }

  ngOnInit() {
    this.auth.currentUser$.subscribe((next) => {
      this.usuario = next;
    });
    this.forumEscolhido$.subscribe((next) => {
      this.forum = next;
      this.carregarDuvidas();
    });
  }

  carregarDuvidas(resetPageIndex: boolean = false) {
    if (!!this.forum) {
      this.isCarregando = true;

      if (resetPageIndex) {
        this.currentPage = 1;
      }

      this.duvidaService.getAll(this.forum.id, null, this.currentPage, this.pageSize).pipe(tap(response => {
        this.dados = response.content;
        this.currentPage = response.currentPage;
        this.totalElements = response.totalElements;
        this.totalPages = response.totalPages;
        this.pageSize = response.pageSize;
        this.isCarregando = false;
      }, _ => {
        this.message.error("Não foi possível recuperar ");
        this.isCarregando = false;
      })).subscribe();
    }
  }

  carregarRespostas(duvida: Duvida) {
    this.duvidaEscolhida$.next(duvida);
  }

  openForm(duvida?: Duvida) {
    if (!duvida) {
      duvida = {forum: this.forum};
    }
    this.openForm$.next(duvida);
  }

  delete(duvida: Duvida) {
    if (duvida.id) {
      this.duvidaService.delete(duvida.id).pipe(tap(_ => {
        this.message.success("Dúvida removida");
        this.carregarDuvidas();
      }, _ => {
        this.message.error("Erro ao remover dúvida");
      })).subscribe();
    }
  }

  canDelete(duvida: Duvida) {
    return this.canEdit(duvida) || this.auth.isCoordenador;
  }

  canEdit(duvida: Duvida) {
    return this.usuario ? duvida.pessoaCadastro.id === this.usuario.id : false;
  }

  recuperarSigla(nome: string) {
    const nomes = nome.split(" ");
    const siglas: string[] = [];
    for (const nom of nomes) {
      siglas.push(nom.charAt(0).toUpperCase());
    }
    return siglas.join(".");
  }

  responderDuvida(duvida: Duvida, resposta?: Resposta) {
    if (!resposta) {
      resposta = {duvida};
    }
    this.respostaCadastro$.next(resposta);
  }

  isDuvidaEscolhida(duvida: Duvida): boolean {
    return !!this.duvidaEscolhida$.value && this.duvidaEscolhida$.value.id === duvida.id;
  }
}
