import {Component, OnInit} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";
import {Tipo} from "../../../../../core/models/tipo.model";
import {tap} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd";
import {RespostaService} from "../../../../../core/services/forum/resposta.service";
import {Resposta} from "../../../../../core/models/forum/resposta.model";
import {FormComponent} from "../../../../../core/components/form.component";

@Component({
  selector: "app-resposta-form",
  templateUrl: "./resposta-form.component.html",
  styleUrls: ["./resposta-form.component.less"]
})
export class RespostaFormComponent extends FormComponent<Resposta> implements OnInit {
  // Estrutura de Dados
  duvida: Tipo;

  constructor(
    private fb: FormBuilder,
    private respostaService: RespostaService,
    protected message: NzMessageService
  ) {
    super(message);
  }

  ngOnInit() {
    this.initializeForm();
    this.openForm.subscribe((next) => {
      if (!next) {
        return;
      }

      this.duvida = next.duvida;
      if (!next.id) {
        this.initializeForm();
      } else {
        this.isEditando = true;
        this.formulario.patchValue({
          id: next.id,
          nome: next.nome
        }, {emitEvent: false});
      }
      this.isVisible = true;
    });
  }

  initializeForm() {
    this.formulario = this.fb.group({
      id: [null],
      nome: [null, Validators.required]
    });
  }

  closeModal() {
    this.initializeForm();
    super.close(this.duvida);
  }

  save() {
    const resposta = super.construirObjeto();

    if (!resposta) {
      return;
    }

    resposta.duvida = this.duvida;

    this.respostaService.save(resposta).pipe(tap(_ => {
      this.message.success("Dúvida salva");
      this.closeModal();
    }, _ => {
      this.message.error("Erro ao salvar dúvida");
      this.isSalvando = false;
    })).subscribe();
  }

  delete() {
    this.isSalvando = true;
    const params = this.formulario.getRawValue();
    if (!params.id && this.isEditando) {
      this.respostaService.delete(params.id).pipe(tap(_ => {
        this.message.success("Dúvida removida");
        this.closeModal();
      }, _ => {
        this.message.error("Erro ao remover dúvida");
        this.isSalvando = false;
      })).subscribe();
    }
  }
}
