import {Component, Input, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {Resposta} from "../../../../core/models/forum/resposta.model";
import {User} from "../../../../core/models/auth/user.model";
import {RespostaService} from "../../../../core/services/forum/resposta.service";
import {AuthenticationService} from "../../../../core/services/auth/authentication.service";
import {NzMessageService} from "ng-zorro-antd";
import {tap} from "rxjs/operators";
import {Duvida} from "../../../../core/models/forum/duvida.model";
import {ListComponent} from "../../../../core/components/list.component";

@Component({
  selector: "app-resposta",
  templateUrl: "./resposta.component.html",
  styleUrls: ["./resposta.component.less"]
})
export class RespostaComponent extends ListComponent<Resposta> implements OnInit {
  // Input
  duvida: Duvida;

  // MessageBus
  @Input() duvidaEscolhida$: Observable<Duvida>;

  // Estrutura de Dados
  usuario: User;

  constructor(
    private respostaService: RespostaService,
    private auth: AuthenticationService,
    private message: NzMessageService
  ) {
    super();
  }

  ngOnInit() {
    this.auth.currentUser$.subscribe((next) => {
      this.usuario = next;
    });
    this.duvidaEscolhida$.subscribe((next) => {
      this.duvida = next;
      this.carregarRespostas();
    });
  }

  carregarRespostas(resetPageIndex: boolean = false) {
    if (!!this.duvida) {
      this.isCarregando = true;

      if (resetPageIndex) {
        this.currentPage = 1;
      }

      this.respostaService.getAll(this.duvida.id, null, this.currentPage, this.pageSize).pipe(tap(response => {
        this.dados = response.content;
        this.currentPage = response.currentPage;
        this.totalElements = response.totalElements;
        this.totalPages = response.totalPages;
        this.pageSize = response.pageSize;
        this.isCarregando = false;
      }, _ => {
        this.message.error("Não foi possível recuperar ");
        this.isCarregando = false;
      })).subscribe();
    }
  }

  openForm(resposta?: Resposta) {
    if (!resposta) {
      resposta = {duvida: this.duvida};
    }
    this.openForm$.next(resposta);
  }

  delete(resposta: Resposta) {
    if (resposta.id) {
      this.respostaService.delete(resposta.id).pipe(tap(_ => {
        this.message.success("Dúvida removida");
        this.carregarRespostas();
      }, _ => {
        this.message.error("Erro ao remover dúvida");
      })).subscribe();
    }
  }

  canDelete(resposta: Resposta) {
    return this.canEdit(resposta) || this.auth.isCoordenador;
  }

  canEdit(resposta: Resposta) {
    return this.usuario ? resposta.pessoaCadastro.id === this.usuario.id : false;
  }

  recuperarSigla(nome: string) {
    const nomes = nome.split(" ");
    const siglas: string[] = [];
    for (const nom of nomes) {
      siglas.push(nom.charAt(0).toUpperCase());
    }
    return siglas.join(".");
  }
}
