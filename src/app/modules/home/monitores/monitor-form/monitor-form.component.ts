import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PessoaService} from "../../../../core/services/pessoa/pessoa.service";
import {ValidateService} from "../../../../core/services/utils/validate.service";
import {Observable} from "rxjs";
import {PessoaListItem} from "../../../../core/models/pessoa/pessoa-list-item.model";
import {NzMessageService} from "ng-zorro-antd";
import {Pessoa} from "../../../../core/models/pessoa/pessoa.model";
import {tap} from "rxjs/operators";
import {FormComponent} from "../../../../core/components/form.component";

@Component({
  selector: "app-monitor-form",
  templateUrl: "./monitor-form.component.html",
  styleUrls: ["./monitor-form.component.less"]
})
export class MonitorFormComponent extends FormComponent<PessoaListItem> implements OnInit {
  // Estrutura de Dados
  telefones: string[] = [];

  constructor(
    private fb: FormBuilder,
    private validateService: ValidateService,
    protected message: NzMessageService,
    private pessoaService: PessoaService
  ) {
    super(message);
    this.initializeForm();
  }

  ngOnInit() {
    this.openForm.subscribe(next => {
      if (!next) {
        return;
      }

      this.initializeForm();

      if (!!next.id) {
        this.pessoaService.findOne(next.id).pipe(tap(pessoa => {
          this.formulario.patchValue({...pessoa}, {emitEvent: false});
          this.telefones.push(...pessoa.telefones);
          this.isEditando = true;
        }, _ => {
          this.message.error("Erro ao recuperar monitor");
          this.close();
          return;
        })).subscribe();
      }

      this.isVisible = true;
    });
  }

  initializeForm() {
    this.formulario = this.fb.group({
      id: [null],
      matricula: [null],
      nome: [null, Validators.required],
      dataNascimento: [null, Validators.required],
      cpf: [null],
      rg: [null],
      email: [null, Validators.email],
      usuario: [null, Validators.required],
      senha: [null, Validators.required],
      permissaoCoordenador: [false],
      telefone: [null]
    });

    // TODO buscar por cpf, rg ou usuario e verificar se já existe
    this.formulario.get("cpf").valueChanges.subscribe(cpf => {
      if (cpf) {
        this.formulario.patchValue({cpf: this.validateService.formatarCPF(cpf)}, {emitEvent: false});
      }

      if (!!cpf && cpf.length === 14) {
        if (!this.validateService.validarCPF(cpf)) {
          this.formulario.get("cpf").setValue(null, {emitEvent: false});
        }
      }
    });

    this.formulario.get("rg").valueChanges.subscribe(rg => {
      this.formulario.get("rg").setValue(this.validateService.formatarRG(rg), {emitEvent: false});
    });

    this.formulario.get("telefone").valueChanges.subscribe(tel => {
      if (!!tel) {
        this.formulario.get("telefone").setValue(this.validateService.formatarTelefone(tel), {emitEvent: false});
      }
    });
  }

  addTelefone() {
    const params = this.formulario.getRawValue();
    if (!!params.telefone) {
      if (params.telefone.length > 12) {
        this.telefones.push(params.telefone);
        this.formulario.get("telefone").setValue(null, {emitEvent: false});
      }
    }
  }

  deleteTelefone(telefone: string) {
    const index = this.telefones.indexOf(telefone);
    this.telefones.splice(index, 1);
  }

  salvar() {
    const pessoa = super.construirObjeto();
    if (!pessoa) {
      return;
    }

    pessoa.telefones = this.telefones;

    this.pessoaService.saveMonitor(pessoa).pipe(tap(_ => {
      this.message.success("Monitor salvo");
      this.close();
    }, _ => {
      this.message.error("Erro ao salvar monitor");
      this.isSalvando = false;
    })).subscribe();
  }

  deletar() {
    const params = this.formulario.getRawValue();
    const idMonitor = params.id;
    if (!!idMonitor) {
      this.isSalvando = true;
      this.pessoaService.deleteMonitor(idMonitor).pipe(tap(_ => {
        this.message.success("Monitor desativado");
        this.close();
      }, _ => {
        this.message.error("Erro ao desativar monitor");
        this.isSalvando = false;
      })).subscribe();
    }
  }

  close() {
    this.initializeForm();
    this.telefones = [];
    super.close();
  }
}
