import {NgModule} from "@angular/core";
import {MonitorComponent} from "./monitor.component";
import {RouterModule, Routes} from "@angular/router";
import {PessoaService} from "../../../core/services/pessoa/pessoa.service";
import {
  NzButtonModule,
  NzDatePickerModule,
  NzDrawerModule,
  NzFormModule,
  NzIconModule,
  NzInputModule, NzPopconfirmModule,
  NzSpinModule, NzSwitchModule,
  NzTableModule
} from "ng-zorro-antd";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";
import {MonitorFormComponent} from "./monitor-form/monitor-form.component";
import {ValidateService} from "../../../core/services/utils/validate.service";

const routes: Routes = [
  {
    path: "",
    component: MonitorComponent,
    children: []
  }
];

@NgModule({
  declarations: [
    MonitorComponent,
    MonitorFormComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    NzTableModule,
    CommonModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzButtonModule,
    NzIconModule,
    NzDrawerModule,
    NzSpinModule,
    NzDatePickerModule,
    NzSwitchModule,
    NzPopconfirmModule
  ],
  providers: [
    PessoaService,
    ValidateService
  ]
})
export class MonitorModule {
  constructor() {}
}
