import {Component, OnInit} from "@angular/core";
import {PessoaService} from "../../../core/services/pessoa/pessoa.service";
import {tap} from "rxjs/operators";
import {PessoaListItem} from "../../../core/models/pessoa/pessoa-list-item.model";
import {FormBuilder} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd";
import {ListComponent} from "../../../core/components/list.component";

@Component({
  selector: "app-monitor",
  templateUrl: "./monitor.component.html",
  styleUrls: ["./monitor.component.less"]
})
export class MonitorComponent extends ListComponent<PessoaListItem> implements OnInit {
  constructor(
    private fb: FormBuilder,
    private message: NzMessageService,
    private pessoaService: PessoaService
  ) {
    super();
  }

  ngOnInit() {
    this.initializeFiltroForm();
    this.buscarMonitores();
  }

  initializeFiltroForm() {
    this.filtroForm = this.fb.group({
      search: [null]
    });
    this.filtroForm.get("search").valueChanges.subscribe(next => {
      if (!next) {
        this.search = null;
        this.pageFiltroChange(1);
      } else if (next.length > 3) {
        this.search = next;
        this.pageFiltroChange(1);
      }
    });
  }

  buscarMonitores() {
    this.isCarregando = true;
    this.pessoaService
      .findAll(
        true,
        this.search,
        this.currentPage,
        this.pageSize
      ).pipe(
      tap(
        response => {
          this.pageSize = response.pageSize;
          this.totalElements = response.totalElements;
          this.currentPage = response.currentPage;
          this.totalPages = response.totalPages;
          this.dados = response.content;
          this.isCarregando = false;
        }, err => {
          this.isCarregando = false;
          console.log(err);
        })).subscribe();
  }

  pageFiltroChange(event: number, resetIndex: boolean = false) {
    if (resetIndex) {
      this.pageSize = event;
      this.currentPage = 1;
    } else {
      this.currentPage = event;
    }
    this.buscarMonitores();
  }

  recuperarTelefones(telefones?: string[]): string {
    if (!telefones) {
      return "-";
    }

    return telefones.join(", ");
  }

  openForm(monitor?: PessoaListItem) {
    if (!monitor) {
      monitor = {};
    }
    this.openForm$.next(monitor);
  }

  deletar(idMonitor: number) {
    if (!!idMonitor) {
      this.pessoaService.deleteMonitor(idMonitor).pipe(tap(_ => {
        this.message.success("Monitor desativado");
        this.buscarMonitores();
      }, _ => {
        this.message.error("Erro ao desativar monitor");
      })).subscribe();
    }
  }

  reativar(idMonitor: number) {
    if (!!idMonitor) {
      this.pessoaService.reativar(idMonitor).pipe(tap(_ => {
        this.message.success("Monitor ativado");
        this.buscarMonitores();
      }, _ => {
        this.message.error("Erro ao ativar monitor");
      })).subscribe();
    }
  }
}
