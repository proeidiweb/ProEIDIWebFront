import {NgModule} from "@angular/core";
import {HomeComponent} from "./home.component";
import {NzIconModule, NzLayoutModule, NzMenuModule, NzToolTipModule} from "ng-zorro-antd";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    children: [
      {
        path: "forum",
        loadChildren: () => import("./forum/forum.module").then(m => m.ForumModule)
      },
      {
        path: "monitor",
        loadChildren: () => import("./monitores/monitor.module").then(m => m.MonitorModule)
      },
      {
        path: "periodo-matricula",
        loadChildren: () => import("./periodo-matricula/periodo-matricula.module").then(m => m.PeriodoMatriculaModule)
      },
      {
        path: "sala",
        loadChildren: () => import("./sala/sala.module").then(m => m.SalaModule)
      },
      {
        path: "disciplina",
        loadChildren: () => import("./disciplina/disciplina.module").then(m => m.DisciplinaModule)
      }
    ]
  }
];

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    NzLayoutModule,
    NzMenuModule,
    RouterModule.forChild(routes),
    NzIconModule,
    NzToolTipModule
  ],
  providers: []
})
export class HomeModule {}
