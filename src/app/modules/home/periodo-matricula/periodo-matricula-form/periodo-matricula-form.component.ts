import {Component, OnInit} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";
import {PeriodoMatricula} from "../../../../core/models/matricula/periodo-matricula.model";
import {tap} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd";
import {PeriodoMatriculaService} from "../../../../core/services/matricula/periodo-matricula.service";
import {FormComponent} from "../../../../core/components/form.component";

@Component({
  selector: "app-periodo-matricula-form",
  templateUrl: "./periodo-matricula-form.component.html",
  styleUrls: ["./periodo-matricula-form.component.less"]
})
export class PeriodoMatriculaFormComponent extends FormComponent<PeriodoMatricula> implements OnInit {
  constructor(
    private fb: FormBuilder,
    protected message: NzMessageService,
    private periodoService: PeriodoMatriculaService
  ) {
    super(message);
  }

  ngOnInit() {
    this.initializeForm();
    this.openForm.subscribe(next => {
      if (!next) {
        return;
      }

      this.initializeForm();
      if (!!next) {
        this.formulario.patchValue({...next}, {emitEvent: false});
      }
      this.isVisible = true;
    });
  }

  initializeForm() {
    this.formulario = this.fb.group({
      id: [""],
      dataInicio: ["", Validators.required],
      dataFim: ["", Validators.required]
    });
  }

  save() {
    const periodo = super.construirObjeto();

    if (!periodo) {
      return;
    }

    this.periodoService.save(periodo).pipe(tap(_ => {
      this.message.success("Período de matrícula salvo");
      this.close();
    }, err => {
      this.message.error(err.message);
      this.isSalvando = false;
    })).subscribe();
  }

  close() {
    this.initializeForm();
    super.close();
  }
}
