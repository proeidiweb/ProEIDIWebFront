import {NgModule} from "@angular/core";
import {PeriodoMatriculaService} from "../../../core/services/matricula/periodo-matricula.service";
import {RouterModule, Routes} from "@angular/router";
import {PeriodoMatriculaComponent} from "./periodo-matricula.component";
import {
  NzButtonModule,
  NzDatePickerModule,
  NzFormModule,
  NzIconModule,
  NzModalModule,
  NzPopconfirmModule,
  NzSpinModule,
  NzTableModule
} from "ng-zorro-antd";
import {CommonModule} from "@angular/common";
import {PeriodoMatriculaFormComponent} from "./periodo-matricula-form/periodo-matricula-form.component";
import {ReactiveFormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: "",
    component: PeriodoMatriculaComponent,
    children: []
  }
];

@NgModule({
  declarations: [
    PeriodoMatriculaComponent,
    PeriodoMatriculaFormComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    NzTableModule,
    NzButtonModule,
    NzIconModule,
    NzPopconfirmModule,
    CommonModule,
    NzSpinModule,
    NzModalModule,
    NzFormModule,
    ReactiveFormsModule,
    NzDatePickerModule,
  ],
  providers: [
    PeriodoMatriculaService
  ]
})
export class PeriodoMatriculaModule {
  constructor() {}
}
