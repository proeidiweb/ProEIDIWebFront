import {Component, OnInit} from "@angular/core";
import {PeriodoMatriculaService} from "../../../core/services/matricula/periodo-matricula.service";
import {PeriodoMatricula} from "../../../core/models/matricula/periodo-matricula.model";
import {tap} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd";
import {BehaviorSubject} from "rxjs";
import {ListComponent} from "../../../core/components/list.component";

@Component({
  selector: "app-periodo-matricula",
  templateUrl: "./periodo-matricula.component.html",
  styleUrls: ["./periodo-matricula.component.less"]
})
export class PeriodoMatriculaComponent extends ListComponent<PeriodoMatricula> implements OnInit {
  constructor(
    private periodoMatriculaService: PeriodoMatriculaService,
    private message: NzMessageService
  ) {
    super();
  }

  ngOnInit() {
    this.buscarPeriodos();
  }

  buscarPeriodos() {
    this.isCarregando = true;
    this.periodoMatriculaService.findAll(this.currentPage, this.pageSize).pipe(tap((response) => {
      this.dados = response.content;
      this.pageSize = response.pageSize;
      this.currentPage = response.currentPage;
      this.totalElements = response.totalElements;
      this.totalPages = response.totalPages;
      this.isCarregando = false;
    }, _ => {
      this.message.error("Erro ao recuperar períodos de matrícula");
    })).subscribe();
  }

  pageFiltroChange(event: number, resetIndex: boolean = false) {
    if (resetIndex) {
      this.pageSize = event;
      this.currentPage = 1;
    } else {
      this.currentPage = event;
    }
    this.buscarPeriodos();
  }

  openForm(periodo?: PeriodoMatricula) {
    if (!periodo) {
      periodo = {};
    }
    this.openForm$.next(periodo);
  }

  desativar(idPeriodoMatricula: number) {
    this.periodoMatriculaService.desativar(idPeriodoMatricula).pipe(tap(_ => {
      this.message.success("Período de matrícula desativado");
      this.buscarPeriodos();
    }, _ => {
      this.message.error("Não foi possível desativar o período de matrícula");
    })).subscribe();
  }
}
