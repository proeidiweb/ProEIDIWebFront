import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../core/services/auth/authentication.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.less"]
})
export class HomeComponent implements OnInit {
  public collapsed = false;

  constructor(
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit(): void {
  }

  openUrl(url: string) {
    this.router.navigateByUrl(url);
  }

  get isCoordenador() {
    return this.auth.isCoordenador;
  }

  get user$() {
    return this.auth.currentUser$;
  }

  logout() {
    this.auth.logout();
  }

  collapse() {
    this.collapsed = !this.collapsed;
  }
}
