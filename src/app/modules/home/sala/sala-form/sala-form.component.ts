import {Component, OnInit} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";
import {Sala} from "../../../../core/models/matricula/sala.model";
import {tap} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd";
import {SalaService} from "../../../../core/services/matricula/sala.service";
import {FormComponent} from "../../../../core/components/form.component";

@Component({
  selector: "app-sala-form",
  templateUrl: "./sala-form.component.html",
  styleUrls: ["./sala-form.component.less"]
})
export class SalaFormComponent extends FormComponent<Sala> implements OnInit {

  constructor(
    private fb: FormBuilder,
    protected message: NzMessageService,
    private salaService: SalaService
  ) {
    super(message);
  }

  ngOnInit() {
    this.initializeForm();

    this.openForm.subscribe((next) => {
      if (!next) {
        return;
      }

      this.initializeForm();
      if (!!next.id) {
        this.formulario.patchValue({...next}, {emitEvent: false});
        this.isEditando = true;
      }
      this.isVisible = true;
    });
  }

  initializeForm() {
    this.formulario = this.fb.group({
      id: [null],
      nome: [null, Validators.required],
      descricao: [null, Validators.required]
    });
  }

  save() {
    const sala = super.construirObjeto();

    if (!sala) {
      return;
    }

    this.salaService.save(sala).pipe(tap(_ => {
      this.message.success("Sala salva");
      this.close();
    }, err => {
      this.message.error(err.message);
      this.isSalvando = false;
    })).subscribe();
  }

  desativar() {
    const idSala = this.formulario.getRawValue().id;
    this.isSalvando = true;
    this.salaService.desativar(idSala).pipe(tap(_ => {
      this.message.success("Sala desativada");
      this.close();
    }, err => {
      this.message.error("Não foi possível desativar a sala");
      console.log(err.message);
    })).subscribe();
  }

  close() {
    this.initializeForm();
    super.close();
  }
}
