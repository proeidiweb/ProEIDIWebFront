import {Component, OnInit} from "@angular/core";
import {Sala} from "../../../core/models/matricula/sala.model";
import {FormBuilder} from "@angular/forms";
import {SalaService} from "../../../core/services/matricula/sala.service";
import {tap} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd";
import {ListComponent} from "../../../core/components/list.component";

@Component({
  selector: "app-sala",
  templateUrl: "./sala.component.html",
  styleUrls: ["./sala.component.less"]
})
export class SalaComponent extends ListComponent<Sala> implements OnInit {

  constructor(
    private salaService: SalaService,
    private message: NzMessageService,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    this.initializeForm();
    this.carregarSalas(1);
  }

  initializeForm() {
    this.filtroForm = this.fb.group({
      search: [null]
    });

    this.filtroForm.get("search").valueChanges.subscribe(next => {
      if (!!next && next.length > 0) {
        this.search = next;
      } else {
        this.search = null;
      }
      this.carregarSalas(1);
    });
  }

  carregarSalas(current: number, resetIndex: boolean = false) {
    if (!!resetIndex) {
      this.currentPage = 1;
      this.pageSize = current;
    } else {
      this.currentPage = current;
    }

    this.isCarregando = true;

    this.salaService.findAllPageable(this.currentPage, this.pageSize, this.search).pipe(
      tap(response => {
        this.totalElements = response.totalElements;
        this.totalPages = response.totalPages;
        this.dados = response.content;
        this.isCarregando = false;
      }, err => {
        this.message.error("Erro ao recuperar as salas");
        this.isCarregando = false;
        console.log(err.message);
      })).subscribe();
  }

  desativar(idSala: number) {
    this.salaService.desativar(idSala).pipe(tap(_ => {
      this.message.success("Sala desativada");
      this.carregarSalas(1);
    }, err => {
      this.message.error("Não foi possível desativar a sala");
      console.log(err.message);
    })).subscribe();
  }

  openForm(sala?: Sala) {
    if (!sala) {
      sala = {};
    }
    this.openForm$.next(sala);
  }
}
