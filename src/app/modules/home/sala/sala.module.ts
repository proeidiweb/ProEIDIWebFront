import {NgModule} from "@angular/core";
import {SalaComponent} from "./sala.component";
import {SalaService} from "../../../core/services/matricula/sala.service";
import {
  NzButtonModule,
  NzFormModule,
  NzIconModule,
  NzInputModule,
  NzModalModule,
  NzPopconfirmModule,
  NzSpinModule,
  NzTableModule
} from "ng-zorro-antd";
import {ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {RouterModule, Routes} from "@angular/router";
import {SalaFormComponent} from "./sala-form/sala-form.component";

const routes: Routes = [
  {
    path: "",
    component: SalaComponent,
    children: []
  }
];

@NgModule({
  declarations: [
    SalaComponent,
    SalaFormComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    ReactiveFormsModule,
    NzTableModule,
    NzPopconfirmModule,
    NzIconModule,
    CommonModule,
    NzModalModule,
    NzSpinModule
  ],
  providers: [
    SalaService
  ]
})
export class SalaModule {
  constructor() {}
}
