import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd";
import {AuthenticationService} from "../../../core/services/auth/authentication.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.less"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private message: NzMessageService,
    private auth: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      login: ["", Validators.required],
      senha: ["", Validators.required]
    });
  }

  handleSubmit() {
    if (this.loginForm.invalid) {
      this.message.create("error", "Formulário inválido");
      return;
    }
    const formData = { ...this.loginForm.value };

    this.loading = true;

    this.auth.login(formData.login, formData.senha).subscribe(
      it => {
        this.loading = false;
      },
      err => {
        this.loading = false;
      }
    );
  }

}
