import {AuthComponent} from "./auth.component";
import {LoginComponent} from "./login/login.component";
import {NgModule} from "@angular/core";
import {PapelSelectComponent} from "./papel-select/papel-select.component";
import {RouterModule} from "@angular/router";
import {NzButtonModule, NzCardModule, NzFormModule, NzIconModule, NzInputModule} from "ng-zorro-antd";
import {ReactiveFormsModule} from "@angular/forms";
import {AuthRoutingModule} from "./auth-routing.module";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [AuthComponent, LoginComponent, PapelSelectComponent],
  imports: [
    AuthRoutingModule,
    RouterModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzIconModule,
    NzButtonModule,
    NzCardModule,
    CommonModule
  ]
})
export class AuthModule {}
