import {Component, OnInit} from "@angular/core";
import {AuthenticationService} from "../../../core/services/auth/authentication.service";
import {Tipo} from "../../../core/models/tipo.model";

@Component({
  selector: "app-papel-select",
  templateUrl: "./papel-select.component.html",
  styleUrls: ["./papel-select.component.less"]
})
export class PapelSelectComponent implements OnInit {

  papeis: Tipo[];

  constructor(private auth: AuthenticationService) {}

  ngOnInit(): void {
    this.auth.papeis$.subscribe(next => (this.papeis = next));
  }

  selecionarPapel(papel: Tipo) {
    this.auth.selecionarPapel(papel.id).subscribe(_ => {}, _ => {});
  }

  logout() {
    this.auth.logout();
  }

}
