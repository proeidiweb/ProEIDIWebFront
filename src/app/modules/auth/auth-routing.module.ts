import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {PapelSelectComponent} from "./papel-select/papel-select.component";
import {AuthComponent} from "./auth.component";

const routes: Routes = [
  {
    path: "",
    component: AuthComponent,
    children: [
      {
        path: "login",
        component: LoginComponent,
        // canActivate: [NotAuthenticatedGuard]
      },
      {
        path: "papeis",
        component: PapelSelectComponent,
        // canActivate: [AuthenticatedGuard],
        data: {
          auth: {
            anyToken: true
          }
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
