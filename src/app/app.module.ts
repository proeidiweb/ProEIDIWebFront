import {BrowserModule} from "@angular/platform-browser";
import {ApplicationRef, LOCALE_ID, NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NZ_I18N, pt_BR} from "ng-zorro-antd/i18n";
import {registerLocaleData} from "@angular/common";
import pt from "@angular/common/locales/pt";
import {NzFormModule, NzIconModule, NzInputModule, NzMessageModule} from "ng-zorro-antd";
import {environment} from "../environments/environment";
import {ADMIN_API_URL, CODIGO_APP, CORE_API_URL, OPEN_ID_URL, TOKEN_STORAGE_KEY} from "./core/core.constants";
import {CoreModule} from "./core/core.module";

registerLocaleData(pt, "pt-br");

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzMessageModule,
    ReactiveFormsModule,
    NzInputModule,
    NzIconModule,
    NzFormModule,
    CoreModule.forRoot()
  ],
  providers: [
    {provide: NZ_I18N, useValue: pt_BR},
    {provide: LOCALE_ID, useValue: "pt-BR"},
    {provide: CORE_API_URL, useValue: environment.coreApiUrl},
    {provide: OPEN_ID_URL, useValue: environment.openIdUrl},
    {provide: TOKEN_STORAGE_KEY, useValue: "proeidiweb-token"},
    {provide: CODIGO_APP, useValue: environment.codigoApp},
    {provide: ADMIN_API_URL, useValue: environment.adminApiUrl}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(public appRef: ApplicationRef) {
  }
}
