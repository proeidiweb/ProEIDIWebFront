export const environment = {
  production: false,
  coreApiUrl: "http://192.168.1.13:9090",
  openIdUrl: "http://192.168.1.13:9090",
  adminApiUrl: "https://test.backend.proeidiweb.imd.ufrn.br/admin",
  codigoApp: "proeidi",
};
