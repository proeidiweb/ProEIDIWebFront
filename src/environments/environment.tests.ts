export const environment = {
  production: false,
  coreApiUrl: "https://test.proeidiweb.imd.ufrn.br",
  openIdUrl: "https://test.proeidiweb.imd.ufrn.br",
  adminApiUrl: "https://test.backend.proeidiweb.imd.ufrn.br/admin/",
  codigoApp: "proeidi",
};
