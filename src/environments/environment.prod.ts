export const environment = {
  production: true,
  coreApiUrl: "https://backend.proeidiweb.imd.ufrn.br",
  openIdUrl: "https://backend.proeidiweb.imd.ufrn.br",
  adminApiUrl: "https://backend.proeidiweb.imd.ufrn.br/admin",
  codigoApp: "proeidiweb",
};
